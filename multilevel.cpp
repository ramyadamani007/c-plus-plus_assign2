/********************
	Include
********************/

#include<iostream>

using namespace std;

/***** Class 1 declaration *****/

class parent			/***** 1st parent class *****/
{
	public:
 		int x;
 		void getdata()
 		{
    		cout << "Enter value of x= ";
    		cin >> x;
 		}
};

/***** Class 2 declaration *****/

class child : public parent	/***** child class *****/	/***** 2nd parent class for 2nd child class *****/  
{
 	public:
	 	int y;
	 	void readdata()
	 	{
	 	    cout << "\nEnter value of y= "; 
	 	    cin >> y;
	 	}
};

/***** Class 3 declaration *****/

class child2 : public child	/***** 2nd child class ****/
{
	private:
 		int z;
 	public:
	 	void indata()
	 	{
	    		cout << "\nEnter value of z= ";
	    		cin >> z;
	 	}
	 	void product()
	 	{
	 	    cout << "\nProduct= " << x * y * z;
	 	}
};



int main()
{
	child2 a;
	a.getdata();
	a.readdata();
	a.indata();
	a.product();
	cout << "\n" << endl;
        return 0;
} 












