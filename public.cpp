/********************
	Include
********************/

#include<iostream>

using namespace std;

/***** Class 1 declaration *****/
class base			/***** parent class *****/
{
	private:
		int x = 100;
	
	protected:
		int y = 10;
		
	public:
		int z = 1;
		int getprivate()
		{
			return x;
		}
};

/***** Class 2 declaration *****/

class derieved : public base	/***** child class *****/
{
	public:
		int getprot()
		{
			return y;
		}
}d1;

/***** MAIN *****/

int main()
{
	cout << "Private = "<< d1.getprivate() <<endl;
	cout << "Protected = "<< d1.getprot() <<endl;
	cout << "Public = "<< d1.z <<endl;
	return 0;
}






	
