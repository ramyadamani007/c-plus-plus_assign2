/********************
	Include
********************/

#include<iostream>
using namespace std;

/***** Class 1 declaration *****/
class mother			/***** parent class *****/
{
	public:
		void disp()
		{
			cout<<"Mother (Base) class"<<endl;	
		}
};


/***** Class 2 declaration *****/

class daughter : public mother	/***** child class *****/
{
	public:
		void display()
		{
			cout<<"Daughter (derieved) class"<<endl;
		}
}d1;

int main()
{

	d1.disp();
	d1.display();
	return 0;
}









