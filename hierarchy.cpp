/********************
	Include
********************/

#include<iostream>
using namespace std;


/***** Class declaration *****/

class add
{
	private :
		int a,b,c;
	public:
  		add(int x,int y)
		{
			a=x;
			b=y;
			cout<<"constructor 1"<<endl;
		}
		
		
		add (int z)
		{
			c=z;
			cout<<"constructor 2"<<endl;
		}
		
		add ()
		{
			cout<<"constructor 3"<<endl;
		}

		~add()
		{
			cout<<"destructor"<<endl;
		}

};

/***** MAIN *****/

int main()
{
	add obj1(25,34), obj2(17), obj;
	return 0;
}






