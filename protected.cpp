/********************
	Include
********************/

#include<iostream>

using namespace std;

/***** Class 1 declaration *****/
class car			/***** parent class *****/
{
	private:
		int x = 180, y = 280;
		
	protected: 
		void getdata (int hp, int maxspeed)
		{
			hp = x;
			maxspeed = y;
		}
		
	public :
		void showdata ()
		{
			int hp,maxspeed;
			getdata (hp, maxspeed);
			cout << "Horsepower of car is "<< x << " KW ."<< endl;
			cout << "Max speed of car is "<< y << endl;
		}
}c1;

/***** Class 2 declaration *****/

class racecar : protected car
{
		
	public : 
		static void race_car ()
		{
			int rpm = 6000;
			cout << "Revolutions per minute is " << rpm << endl;
		}
}r1;

/***** MAIN *****/

int main()
{
	c1.showdata();
	cout << "The specs of racecar is " << r1.race_car << endl;
	return 0;
}

				
	
	
	
	
	
	
	
	
			
