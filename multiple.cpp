/********************
	Include
********************/

#include<iostream>

using namespace std;

/***** Class 1 declaration *****/

class A			/***** 1st parent class *****/
{
	protected:
		int p;
	public:
		A(int x)
		{
			p=x;
		}
		void disp()
		{
			cout << p << endl;
		}
};


/***** Class 2 declaration *****/

class B			/***** 2nd parent class *****/
{
	protected:
		int q;
	public:
		B(int y)
		{
			q=y;
		}
		void disp()
		{
			cout << q << endl;
		}
};

/***** Class 3 declaration *****/

class C : public A, public B	/***** child class *****/
{
	private:
		int r;
	
	public:
		C (int x, int y, int z) : A(x),B(y)
		{
			 r = z;
		}
		
		void disp()
		{
			A :: disp();
			B :: disp();
			cout << r << endl;
		}		
};

/***** MAIN *****/

int main()
{
	C obj (10,20,30);
	obj.disp();
}















